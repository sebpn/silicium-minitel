<div id="top"></div>

<div align="center">
    <figure>
      <a href="https://gitlab.com/sebpn/silicium-minitel">
        <img src="./resources/Minitel1.jpg" alt="Minitel1" width="300" height="354">
      </a>
      <figcaption>CC BY-SA 3.0, <a href="https://commons.wikimedia.org/w/index.php?curid=74723">Lien</a></figcaption>
    </figure>
</div>

# 📗 Table of Contents

[TOC]

# Un service Silicium sur... Minitel :interrobang:

## Le minitel

Le Minitel est l'acronyme de Médium interactif par numérisation d'information téléphonique est créé par les PTT et utilisé de 1980 à 2012.

Le minitel utilise un service de télécommunication : Videotex renommé Télétel. Ce réseau est basé sur Transpac (réseau par commutation de paquets X25 de France Télécom)

L'utilisateur accède au réseau via son <abbr title="Point d'Accés Videotex">PAVI</abbr> en composant un numéro de téléphone spécifique tels que: 36-15, 36-14... (bien prononcer : "trente-six-quinze"  )

L'écran du Minitel est une matrice texte de 25 lignes par 40 colonnes en mode *Vidéotex* (8 nuances de gris) et se base sur un [système de codage](./resources/documentation/videotex-codes.pdf) qui lui est propre. Le mode « *mosaïque* » permet d'afficher des images avec un jeu de caractères graphiques, constitué de 6 gros pixels.

La documentation technique est disponible ici : [STUM](./resources/documentation/stum1b.pdf) (Spécifications Techniques d'Utilisation du Minitel)

## Le service SILICIUM

L'association Silicium a lancé son exposition Whizz machines lors du Toulouse Game Show 2023. Cette exposition est un succès et nous cherchons à l'améliorer afin de proposer au visiteur une approche encore plus attrayante et immersive.

Nous avons proposé un minitel relié au boitier Minimit. Les services proposés par défaut ont vraiment été appréciés et je me suis dit que l'on pourrait avoir notre propre service Silicium.

J'ai deux approches:

- Une connectée utilisant les solutions des fournisseurs

- Une totalement déconnecté afin d'être autonome (pas de Wifi, pas de 4G)

C'est la 1<sup>ére</sup> approche qui sera étudiée ici car Olivier Mével nous a envoyé un message intitulé : "*créez vos propres services Minitel !*" Jean-Arthur Silve a développé le projet MiniPavi pour appeler des services Minitel en utilisant les websockets[^1]. 

L'intérêt est que l'appel au MiniPavi (par websockets) est intégré au MiniMit en tapant `GUIDE` puis `14`. Et qu'au fur et à mesure que des services seront développés ils seront accessibles dans le `GUIDE` (sinon il faut taper l'URL d'accès dans la zone de saisie du miniPavi)

[^1]:Websocket est un protocole réseau qui permet une communication bi-directionnelle entre client et serveur.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

# 🛠 De quoi c'est fait ?

## Le terminal

- Un minitel + interface Minitel/Internet (ex : Minimit)

- L'émulateur sur navigateur

## Le miniPAVI

Je résume en 2 § mais je vous conseille de lire la page dédiée ici : https://www.minipavi.fr/ 

### Serveur

> MiniPavi est 100% logiciel: c'est un Raspberry, avec un dev en PHP (qui sera également prochainement dispo) et un serveur Asterisk avec le module softmodem pour l'accès par téléphone.

### Client

>  **MiniPaviCli.php** : la classe PHP « MiniPaviCli », que tu utilises dans ton script pour faire ton service. C’est cette classe qui assure la communication entre la passerelle MiniPavi et ton service. Et tu n’aura besoin que de ça.

## Le service Minitel

- Des mosaïques pour les images 

- Le point d'entrée du service qui utilise :
  
  - la lib `MiniPaviCli`
  
  - du code d'accès et de mise en forme des informations à afficher

### Les images mosaïques du videotex

#### MiEdit

J'utilise l'éditeur de page minitel MiEdit en ligne ici : https://minitel.cquest.org/

Qui permet de :

- Créer une page : https://minitel.cquest.org/miedit-page.html

- De gérer des pages sauvegardées : https://minitel.cquest.org/miedit-manage.html
  :memo: pensez à les sauvegarder 😉

- d'importer et d'exporter les arbres (format JSON)

![](./resources/MiEdit.png)

<figcaption>L'éditeur de page en action</figcaption>

#### Pour faciliter la transposition image ➡ videotex

Pour des images simples on peut composer seul l'image avec les caractères mosaïques ou "graphics" (cf video https://www.youtube.com/watch?v=WQ9C6MdD4Tk )

Par exemple ce logo en caractères mosaïques :

![](./resources/silicium_vdt.gif)

Pour des images plus complexes je conseille l'utilisation d'un outil de traitement d'image capable de gérer des calques et des symboles sur une grille

- Le document fait 80px de large sur 75px de hauteur (échelle de 1px)

- La grille est rectangulaire, espacement X de 2px, espacement Y de 3px

- Un calque pour l'image d'origine 
  
  - adapter les couleurs de fond et d'écriture / trait
  
  - Sujet suffisamment épais pour être transposer dans la mosaïque

- Un calque pour déposer les symboles

Voici ce que ça donne sous Inkscape :

![](./resources/Inkscape.png)

Le fichier vierge est disponible ici : [videotexSymbols.svg](./resources/videotexSymbols.svg)

- Activer la grille, et le magnétisme

- Dans le calque 1 : 
  
  - l'image à transposer redimensionnée

- Dans le calque 2 : 
  
  - commencer par la case en haut à gauche 
  
  - déposer le symbole qui correspond le mieux à la portion de l'image en cours
  
  - vérifier que les symboles sont dans l'ordre de lecture G➡D 
  
  - grouper les symboles de la ligne, nommer le groupe (ex : `ligne??`)
  
  - etc...

- Ouvrir le fichier en mode texte 
  
  - Chercher le calque : `Calque2_Videotex`

```xml
    <g inkscape:groupmode="layer" id="layer2" inkscape:label="Calque2_Videotex" style="display:inline">
        <use xlink:href="#space" id="use1"/>
        <use xlink:href="#(" id="use1-2" transform="translate(2)"/>
        <use xlink:href="#," id="use1-5" transform="translate(4)"/>
        <use xlink:href="#$" id="use1-6" transform="translate(6)"/>
        <use xlink:href="#space" id="use1-3" transform="translate(8)"/>
    </g>
```

- Dans **MiEdit**
  
  - Ecrire une chaine avec les valeurs des attributs  `xlink:href="#..."`       

Le logo obtenu :

![](./resources/whizz_vdt.gif)

#### ⚠ Quelques points de vigilance

- La résolution de 25 lignes par 40 colonnes

- Le changement des couleurs sur une même ligne

- Si la mosaïque n'est pas utilisée seule (ex: texte, consignes) il sera peut être nécessaire de retirer de l'arbre **MiEdit** :
  
  - Clear screen
  
  - Move cursor to first row, first column

<p align="right">(<a href="#readme-top">back to top</a>)</p>

### Algorithme

Je me suis basé sur le service de `france24` proposé par Jean-Arthur Silve. De même l'algorithme d'un service est expliqué dans le commentaire d'entête du service `miniChat`.

Le programme principal :

* est une *machine à état*. Les transitions étant réalisées par les actions de l'utilisateur.

* appelle le DAO pour obtenir le contenu à afficher

* se charge de l'affichage des pages
  
  * menu
  
  * liste d'article : traitement spécifique en fonction du nombre d'article
  
  * article : traitement spécifique en fonction du contenu (texte seul, texte + image, image seule)

* se charge de la navigation

Le DAO (Data Access Object) va fournir au programme principal : 

* Le format d'un article : un titre ou une mosaïque et une description.

* une liste d'articles (de 0 à plusieurs). 

* Pour un article de texte seul : le DAO contient en dur les textes du titre et de la description (ex : les pages "Présentation", "Ecrivez-nous")

* Pour une liste d'articles de texte seul : Le DAO interroge des URL de flux RSS et traite chaque flux pour obtenir les articles. (ex : les pages "Agenda" et "Catalogue")

* Pour une liste d'articles de texte + image ou d'une image seule : Le DAO traite le contenu d'un fichier XML. (ex : la page "A la une")

#### Transition entre les pages

Le service Minitel s'articule autour d'un ensemble de pages videotex affichées à l'utilisateur. Selon la saisie, une action sera effectuée pour afficher une nouvelle page ou rester sur la page courante.

```mermaid
graph
    1[Accueil] -- Suite --> 2[Menu]
    2 --"? Envoi"--> 3[Liste article]
    3 --"? Envoi"--> 4[Article]
    2 --"? Envoi"--> 4
    4 -- "Retour (précédent)"--> 4
    4 -- "Suite (suivant)"--> 4
    4 -- Sommaire --> 3
    4 -- Sommaire --> 2
    2 -- Sommaire --> 1
```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

#### Séquence

Le script du programme principal débute par un appel à la passerelle MiniPavi par `MiniPaviCli.start()`. Cela permet d'initialiser la classe MiniPavi qui représente l'utilisateur et donne accès à plusieurs variables:

- `step` : l'étape d'exécution, valorisé par le paramètre `urlParams` de la requête http 

- `content` : le tableau de la saisie utilisateur

- `fctn` : la touche de fonction utilisée 

- `context`: l'objet représentant le contexte de l'utilisateur

- `uniqueId`: Identifiant unique de la connexion au niveau de la passerelle

- `remoteAddr`: l'adresse IP de l'utilisateur

- `typesocket`: le type de connexion
  
  - `'websocket'` pour une connexion via websocket
  
  - `'other'` pour une connexion RTC

- `urlParams`: paramètres indiqués dans l'url du script

La suite du traitement est une boucle infinie (`while`) dans la quelle se trouve une série de conditions (`switch/case`)

Chaque page du service regroupe un ensemble de `case` qui correspondent à:

- La construction de la partie fixe de la page,

- (option) la construction de la partie variable de la page

- Initialisation de la commande de saisie

- Traitement de la saisie utilisateur : texte et touche de fonction (<kbd>Envoi</kbd>, <kbd>Suite</kbd>, etc.)

La construction d'une page termine par `break` ou `break 2`:

- `break`: le script va continuer en exécutant le case correspondant à la valeur de la variable `$step`
- `break 2`: le script s'arrête. Typiquement après l'initialisation de la commande de saisie (on attend en effet l'action utilisateur)

La fin du traitement se terminer par l'envoi des informations avec : `MiniPaviCli.send($content,$next,$context='',$echo=true,$cmd=null,$directCall=false)`

Où : 

- `$content` : contenu de la page videotex

- `$next` : prochaine url à être appelée après saisie de l'utilisateur (ou déconnexion)

- `$context` : le contexte de l'utilisateur

- `$echo` : active l'écho

- `$cmd` : commande envoyée au MiniPavi

- `$directCall` : appel directement la prochaine url sans attendre une action utilisateur (limité à 1 utilisation à la fois)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

```mermaid
sequenceDiagram
    actor Utilisateur
    Utilisateur->>+Service: url
    Service->>MiniPaviCli: start()
    loop infinie
        opt step=0
            Note over Service,MiniPaviCli: construction page Accueil
            Service->>Service: step=5, break2
        end
        opt step=5
            Service->>MiniPaviCli: fctn
            opt fctn=Répetition 
                Service->>Service: step=0, break
            end
            opt fctn!=Suite
                Service->>Service: break2
            end
        end
        opt step=10
            Note over Service,MiniPaviCli: contruction Menu principal
        end
        opt step=15
            Service->>MiniPaviCli: createInputTxtCmd(...)
            Service->>Service: step=20, break2
        end
        opt step=20
            Service->>MiniPaviCli: fctn
            opt fctn=Sommaire
                Service->>Service: step=0, break
            end
            Service->>+MiniPaviCli: content[0]
            MiniPaviCli->>-Service: choix
            opt choix
                Note over Service,MiniPaviCli: 'Msg : Choix incorrect!'
                Service->>Service: step=15, break
            end
            Service-->>+DAO: getNews(num)
            DAO-->>-Service: tNews
            opt verifie tNews
                Note over Service,MiniPaviCli: 'Msg : Aucune info!'
                Service->>Service: step=15, break
            end
            Service->>Service: tnews, title
            opt 1seul article
                Service->>Service: currnews=0, page=0
                Service->>Service: step=40, break
            end
        end
        opt step=25
            Note over Service,MiniPaviCli: contruction liste articles (Fixe)
            Service->>Service: currnews=0, page=0
        end
        opt step=26
            Note over Service,MiniPaviCli: contruction liste article (Variable)
        end
        opt step=30
            Service->>MiniPaviCli: createInputTxtCmd(...)
            Service->>Service: step=35, break2
        end
        opt step=35
            Service->>MiniPaviCli: fctn
            opt fctn=Sommaire
                Service->>Service: step=10, break
            end
            opt fctn=Suite ou Retour
                alt  Suite
                    opt
                        Note over Service,MiniPaviCli: 'Msg : Dernière page' 
                        Service->>Service: step=30, break
                    end
                    Service->>Service: page++
                else
                    opt
                        Note over Service,MiniPaviCli: 'Msg : Première page' 
                        Service->>Service: step=30, break
                    end
                    Service->>Service: page--
                end
                Service->>Service: step=26, break
            end
            Service->>+MiniPaviCli: content[0]
            MiniPaviCli->>-Service: choix
            opt choix
                Note over Service,MiniPaviCli: 'Msg : Choix incorrect!' 
                Service->>Service: step=30, break
            end
        end
        opt step=40
            Note over Service,MiniPaviCli: contruction article (Fixe)
        end
        opt step=41
            Note over Service,MiniPaviCli: contruction article (Variable)
            Service->>Service: step=45, break2
        end
        opt step=45
            Service->>MiniPaviCli: fctn
            opt fctn=Sommaire
                alt 1article
                    Service->>Service: step=10
                else
                    Service->>Service: step=25
                end
                 Service->>Service: break
            end
            opt fctn=Suite ou Retour
                alt  Suite
                    opt
                        Note over Service,MiniPaviCli: 'Msg : Dernier article'  
                        Service->>Service: step=45, break2
                    end
                    Service->>Service: currnews++
                else
                    opt
                        Note over Service,MiniPaviCli: 'Msg : Premier article' 
                        Service->>Service: step=45, break2
                    end
                    Service->>Service: currnews--
                end
                Service->>Service: step=41, break
            end
            Service->>Service: break2
        end
    end
    Service->>MiniPaviCli: send(...)
    Service-->>-Utilisateur: page + attente saisie
```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## Les principales fonctionnalités

- Des pages : 
  
  - accueil, menu, liste des articles, un article (ex : texte seul, texte + image)

- La navigation entre les pages
  
  - Gestion des boutons : <kbd>Envoi</kbd>, <kbd>Suite</kbd>, <kbd>Retour</kbd>, <kbd>Sommaire</kbd>

![](./resources/sili_minipavi.gif)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

# 💻 Pour démarrer

## Pré-requis

### Pour le développement

- Un IDE (exemple : [VsCode](https://code.visualstudio.com/) )

### Pour le traitement d'image

- Un outil de traitement d'image utilisant des calques et des gabarits/symboles du jeu de caractères de la mosaïque du minitel (exemple : [Inkskape](https://inkscape.org/))

- Un éditeur de page minitel pour obtenir le fichier VDT : https://minitel.cquest.org/

### Pour exécuter

- Un serveur web + php

- Votre url par exemple: https://www.silicium.org/minipavi/silicium/ peut être appelée :
  
  - soit par websocket : `ws://go.minipavi.fr:8182/url=https://www.silicium.org/minipavi/silicium/`
  
  - soit par l'émulateur Minitel (de miniPavi) : `https://www.minipavi.fr/emulminitel/index.php?url=https://www.silicium.org/minipavi/silicium/`

- Un code de service
  
  - Depuis l'écran d'accueil miniPAVI, tapez : `SILICIUM` + <kbd>Envoi</kbd>

## Code

- Le client du miniPavi est disponible ici : `./minipavi/lib/MiniPaviCli.php`

- Le code du service est ici :
  
  - `./minipavi/silicum/index.php`
  
  - `./minipavi/silicum/dao.php`

- Une page de test du DAO : `./minipavi/silicum/test.php`

- Le contenu des articles avec une image mosaïque : `./minipavi/silicum/aLaUne.xml`

- Les images mosaïques sont ici : `./minipavi/silicum/*.vdt`

<p align="right">(<a href="#readme-top">back to top</a>)</p>

# 👥 Auteur

👤 **Sébastien Périn**

- Rôle: Concepteur et Développeur
- GitLab: ![avatar](https://gitlab.com/uploads/-/system/user/avatar/5863632/avatar.png?width=23) [Sébastien Périn · GitLab](https://gitlab.com/sebpn)

<p align="right">(<a href="#readme-top">back to top</a>)</p>

# 🙏 Remerciements

## miniPAVI

- site minipavi : https://www.minipavi.fr/

- L'émulateur minitel : https://www.minipavi.fr/emulminitel/index.php

- Les sources du client + 3exemples (france24, miniChat, miniSncf): https://github.com/ludosevilla/minipaviCli

:pray:  [Jean-Arthur Silve](mailto:contact@minipavi.fr) 

## Minitel page editor

- Un éditeur de page videotex et émulateur minitel : https://minitel.cquest.org/

- Les sources : https://github.com/Zigazou/miedit

:pray: Frédéric BISSON

## Minimit

- Plaquette du minimit sur la plateforme **multiplié** : https://xn--multipli-i1a.fr/fr/1-3/objets/minimit

- Le minimit : https://xn--multipli-i1a.fr/minimit/

- Les sources du projet global : https://github.com/multiplie-fr/minimit

- Les sources pour l'ESP32 : https://github.com/multiplie-fr/minimit-beta

:pray: Olivier Mével, Cécile Adam & Pascale Moise

<p align="right">(<a href="#readme-top">back to top</a>)</p>

# 📝 License

## Code PHP

Le code est sous License Creative Commons - BY - NC - SA

![](./resources/Cc-by-nc-sa_icon.png)

## Images

### Minitel

**Minitel1.jpg** est en Creative Commons - BY - SA https://commons.wikimedia.org/w/index.php?curid=74723

### Silicium & Whizz

Les images (quel qu'en soit le format de fichier) de **Silicium**, **Silicium whizz machines** , **Silicium whizz2 - mega machines** et **Silicium 32 ans** ne sont pas libres de droits. http://www.silicium.org

<p align="right">(<a href="#readme-top">back to top</a>)</p>
