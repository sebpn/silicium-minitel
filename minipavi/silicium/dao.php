<?php
/**
 * @file dao.php
 * @author Sébastien Périn <sebastien.perin@gmail.com>
 * @version 1.0 Décembre 2023
 *
 * Fonctions : Data Access Object
 * 
 */

 $urlFlux[0]['name']='Présentation';
 $urlFlux[1]['name']='Agenda';
 $urlFlux[2]['name']='Catalogue';
 $urlFlux[3]['name']='Ecrivez-nous';
 $urlFlux[4]['name']='Nos services';
 $urlFlux[5]['name']='A la une';
 $urlFlux[6]['name']='Rejoignez-nous';
 
 // flux rss = XML
 $urlFlux[0]['url']='';	
 $urlFlux[1]['url']='https://www.silicium.org/index.php/component/jevents/modlatest_rss/rss/';
 $urlFlux[2]['url']='https://www.silicium.org/index.php/blog-catalogue?format=feed&type=rss';
 $urlFlux[3]['url']='';
 $urlFlux[4]['url']='';
 $urlFlux[5]['url']='aLaUne.xml';
 $urlFlux[6]['url']='';

 $tRes=array();
 
  
function getNews($idx) {
	global $urlFlux, $tRes;

	if (!isset($urlFlux[$idx]))
		return $tRes;
	
	$i = 0;
	switch ($idx) {
		case 0:
			$tRes[$i]['titre']='Qui sommes-nous ?';
			$tRes[$i]['desc']=" Silicium est une association loi 1901 française, située à Toulouse et fondée en 1994.".VDT_CRLF." Forte d'une dizaine de membres actifs et d'une centaine de sympathisants, elle collecte de vieilles machines pour les faire revivre lors d'expositions interactives.".VDT_CRLF." Cette page a pour but de vous informer et de vous divertir. Elle sert de base de connaissance et de dialogue entre cyber-historiens et techno-amateurs.";
			break;
		case 1:
			getAgendaByUrl($idx, $i);
			break;
		case 2:
			getCatalogueByUrl($idx, $i);
			break;
		case 3:
			$tRes[$i]['titre']='Nos coordonnées';
			$tRes[$i]['desc']=" Téléphone : ".VDT_CRLF."        +33(0)5.61.85.90.33".VDT_CRLF." Messagerie électronique : ".VDT_CRLF."        info : info@silicium.org".VDT_CRLF."        Oueb : webmaster@silicium.org";
			break;
		case 4:
			$tRes[$i]['titre']='Nos services';
			$tRes[$i]['desc']=" * Evénementiel".VDT_CRLF." * Exposition".VDT_CRLF." * Prêt de matériel".VDT_CRLF." * Expertise".VDT_CRLF." * Conférence";
			break;
		case 5:
			getALaUneByUrl($idx, $i);
			break;
		case 6:
			$tRes[$i]['titre']='Participez à l’effort de Silicium';
			$tRes[$i]['desc']=" La cyber-communauté de l’association Silicium permet de créer un lien fort entre tous ses membres. Que vous soyez proche de Toulouse ou très éloignés, vous pourrez participer et trouver votre bonheur.".VDT_CRLF." Silicium est le moyen de partager cette passion des vieux ordinateurs et vieilles consoles. Vous pourrez accéder au fond et disposez d’informations nombreuses voire uniques. vous pourrez élaborer des projets de restauration, faire revivre des machines.".VDT_CRLF."En fédérant les énergies, nous pourrons réussir plus de choses.";
			break;
		}
	return $tRes;
}

function getAgendaByUrl($idx, $i) {
	global $urlFlux, $tRes;

	$fichier = $urlFlux[$idx]['url'];
	$dom = new DOMDocument();
	$libxml_previous_state = libxml_use_internal_errors(true);
	if (!$dom->load($fichier)) {
		return $tRes;
	}
	libxml_clear_errors();
	libxml_use_internal_errors($libxml_previous_state);

	$i=0;
	$itemList = $dom->getElementsByTagName('item');
	$parties = array();
	foreach ($itemList as $item) {
		$titre = $item->getElementsByTagName('title');
		if ($titre->length > 0) {
			$string = $titre->item(0)->nodeValue;
			$parties = explode(" : ", $string);
			$tRes[$i]['desc']="Le ".$parties[0]." ";
			$tRes[$i]['titre']=$parties[1];
		} else {
			$tRes[$i]['titre']='Sans titre';
		}

		$cat = $item->getElementsByTagName('category');
		if ($cat->length > 0) {
			$string = $cat->item(0)->nodeValue;
			if (strcmp($string, "Interne") == 0) {
				$tRes[$i]['desc'] .= "- Réservé aux membres. ";
			} else {
				$tRes[$i]['desc'] .= "- ".$string.". ";
			}			
		}

		$desc = $item->getElementsByTagName('description');
		if ($desc->length > 0) {
			$string = $desc->item(0)->nodeValue;
			$string = trim(strip_tags($string));
			if (strlen($string) > 0) {
				$tRes[$i]['desc'] .= $string;
			}
		} 

		$i++;
	}
}

function getCatalogueByUrl($idx, $i) {
	global $urlFlux, $tRes;

	$fichier = $urlFlux[$idx]['url'];
	$dom = new DOMDocument();
	$libxml_previous_state = libxml_use_internal_errors(true);
	if (!$dom->load($fichier)) {
		return $tRes;
	}
	libxml_clear_errors();
	libxml_use_internal_errors($libxml_previous_state);

	$i=0;
	$itemList = $dom->getElementsByTagName('item');

	foreach ($itemList as $item) {
		$cat = $item->getElementsByTagName('category');
		if ($cat->length > 0) {
			$string = $cat->item(0)->nodeValue;
			if (strcmp($string, "Catalogue") == 0) {
				// Pour ne pas afficher les articles de présentation du catalogue
				continue ;
			} 
		}

		$titre = $item->getElementsByTagName('title');
		if ($titre->length > 0) {
			$string = $titre->item(0)->nodeValue;
			$tRes[$i]['titre']=$string;
		} else {
			$tRes[$i]['titre']='Sans titre';
		}

		$desc = $item->getElementsByTagName('description');
		if ($desc->length > 0) {
			$string = $desc->item(0)->nodeValue;
			$string = trim(strip_tags($string));
			if (strlen($string) > 0) {
				$tRes[$i]['desc'] = $string;
			}
		} 

		$i++;
	}
}

function getALaUneByUrl($idx, $i) {
	global $urlFlux, $tRes;

	$fichier = $urlFlux[$idx]['url'];
	$dom = new DOMDocument();
	$libxml_previous_state = libxml_use_internal_errors(true);
	if (!$dom->load($fichier)) {
		return $tRes;
	}
	libxml_clear_errors();
	libxml_use_internal_errors($libxml_previous_state);

	$i=0;
	$itemList = $dom->getElementsByTagName('item');

	foreach ($itemList as $item) {
		$titre = $item->getElementsByTagName('title');
		if ($titre->length > 0) {
			$string = $titre->item(0)->nodeValue;
			$tRes[$i]['titre']=$string;
		} else {
			$tRes[$i]['titre']='Sans titre';
		}

		$vdt = $item->getElementsByTagName('vdt');
		if ($vdt->length > 0) {
			$string = $vdt->item(0)->nodeValue;
			$tRes[$i]['vdt']=$string;
		} else {
			$tRes[$i]['vdt']='';
		}

		$vdtnbline = $item->getElementsByTagName('vdtnbline');
		if ($vdtnbline->length > 0) {
			$string = $vdtnbline->item(0)->nodeValue;
			$tRes[$i]['vdtnbline']=$string;
		} else {
			$string = file_get_contents($tRes[$i]['vdt']);
			$tRes[$i]['vdtnbline']=strlen($string) / 40;;
		}

		$desc = $item->getElementsByTagName('description');
		if ($desc->length > 0) {
			$string = $desc->item(0)->nodeValue;
			$string = trim(strip_tags($string));
			$tRes[$i]['desc'] = $string;
		} else {
			$tRes[$i]['desc']='';
		}

		$i++;
	}
}


 ?>